package chan.upang.dagupan.ph.imageshuffle;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;


public class MainActivity extends Activity implements View.OnClickListener{

    private Button btn_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_start = (Button) findViewById(R.id.btn_start);
                btn_start.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.btn_start:

                Intent gotoGameActivity= new Intent(MainActivity.this,GameActivity.class);
                startActivity(gotoGameActivity);
                break;
        }

    }
}
