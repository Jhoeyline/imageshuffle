package chan.upang.dagupan.ph.imageshuffle;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class GameActivity extends Activity implements View.OnClickListener{


    ImageView[] imageCells = new ImageView[16];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        imageCells[0] = (ImageView) findViewById(R.id.cell_1);
        imageCells[1] = (ImageView) findViewById(R.id.cell_2);
        imageCells[2] = (ImageView) findViewById(R.id.cell_3);
        imageCells[3] = (ImageView) findViewById(R.id.cell_4);
        imageCells[4] = (ImageView) findViewById(R.id.cell_5);
        imageCells[5] = (ImageView) findViewById(R.id.cell_6);
        imageCells[6] = (ImageView) findViewById(R.id.cell_7);
        imageCells[7] = (ImageView) findViewById(R.id.cell_8);
        imageCells[8] = (ImageView) findViewById(R.id.cell_9);
        imageCells[9] = (ImageView) findViewById(R.id.cell_10);
        imageCells[10] = (ImageView) findViewById(R.id.cell_11);
        imageCells[11] = (ImageView) findViewById(R.id.cell_12);
        imageCells[12] = (ImageView) findViewById(R.id.cell_13);
        imageCells[13] = (ImageView) findViewById(R.id.cell_14);
        imageCells[14] = (ImageView) findViewById(R.id.cell_15);
        imageCells[15] = (ImageView) findViewById(R.id.cell_16);

        for (int index = 0; index < imageCells.length; index++) {
            imageCells[index].setBackgroundResource(R.drawable.aa);

            imageCells[index].setOnClickListener(this);
        }
        imageCells[0].setImageResource(R.drawable.a);
        imageCells[1].setImageResource(R.drawable.b);
        imageCells[2].setImageResource(R.drawable.c);
        imageCells[3].setImageResource(R.drawable.d);
        imageCells[4].setImageResource(R.drawable.e);
        imageCells[5].setImageResource(R.drawable.f);
        imageCells[6].setImageResource(R.drawable.g);
        imageCells[7].setImageResource(R.drawable.h);
        imageCells[8].setImageResource(R.drawable.i);
        imageCells[9].setImageResource(R.drawable.j);
        imageCells[10].setImageResource(R.drawable.k);
        imageCells[11].setImageResource(R.drawable.l);
        imageCells[12].setImageResource(R.drawable.m);
        imageCells[13].setImageResource(R.drawable.aa);
        imageCells[14].setImageResource(R.drawable.aaa);
        imageCells[15].setImageResource(R.drawable.download);


        imageCells[0].setTag(R.drawable.a);
        imageCells[1].setTag(R.drawable.b);
        imageCells[2].setTag(R.drawable.c);
        imageCells[3].setTag(R.drawable.d);
        imageCells[4].setTag(R.drawable.e);
        imageCells[5].setTag(R.drawable.f);
        imageCells[6].setTag(R.drawable.g);
        imageCells[7].setTag(R.drawable.h);
        imageCells[8].setTag(R.drawable.i);
        imageCells[9].setTag(R.drawable.j);
        imageCells[10].setTag(R.drawable.k);
        imageCells[11].setTag(R.drawable.l);
        imageCells[12].setTag(R.drawable.m);
        imageCells[13].setTag(R.drawable.aa);
        imageCells[14].setTag(R.drawable.aaa);
        imageCells[15].setTag(R.drawable.download);

    }

    private boolean firstpress = true;
    private int firstpressItem = 0;
    private int secondpressItem = 0;
    private int imageResourceA = 0;
    private int imageResourceB = 0;


    @Override
    public void onClick(View view) {
        if (firstpress) {
            firstpress = false;
            firstpressItem = view.getId();
            imageResourceA = (Integer)((ImageView) findViewById(firstpressItem)).getTag();

        } else {

            secondpressItem = view.getId();

            imageResourceA=(Integer)((ImageView) findViewById(firstpressItem)).getTag();
            imageResourceB=(Integer)((ImageView) findViewById(secondpressItem)).getTag();

            ImageView itemA= (ImageView) findViewById(firstpressItem);
            ImageView itemB= (ImageView) findViewById(secondpressItem);

            itemB.setImageResource(imageResourceA);
            itemB.setTag(imageResourceA);

            itemA.setImageResource(imageResourceB);
            itemA.setTag(imageResourceB);
            firstpress=true;


        }
    }
}
